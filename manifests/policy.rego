package main

name = input.metadata.name

required_deployment_labels {
        input.metadata.labels["app"]
}

deny[msg] {
  input.kind = "Deployment"
  not required_deployment_labels
  msg = sprintf("%s must include Kubernetes recommended labels: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/#labels", [name])
}

deny[msg] {
  input.kind = "Deployment"
  not input.spec.selector.matchLabels.app
  msg = "Containers must provide app label for pod selectors"
}
variables:
  SONAR_ARGS: "-Dsonar.projectName=${PROJECT_NAME} -Dsonar.projectKey=some.org:${PROJECT_NAME} -Dsonar.host.url=$SONAR_URL -Dsonar.sources=. -Dsonar.login=$SONAR_TOKEN -Dsonar.coverage.exclusions=node_modules/*"
  PROJECT_NAME: "angular-demo-userx"
  SONAR_URL: "http://34.127.92.40:30090/"
  IMAGE_NAME: "angular-demo-MAGALLI"
  DEPLOYMENT_NAME: "angular-demo-MAGALLI"
  NAMESPACE: "userx"
  SERVICE_PORT: "80"
  INGRESS_HOST: "angular-demo.34-127-8-244.nip.io"
  PROMOTE_SHA: "yes"

